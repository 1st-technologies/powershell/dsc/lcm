[CmdletBinding()]
param(
    [Parameter(Mandatory = $false)]
    [ValidateNotNullOrEmpty()]
    [string]$Thumbprint
)


Write-Verbose -Message "Getting existing AgentID..."
$oldguid = (Get-DscLocalConfigurationManager).AgentId
If($oldguid.Length -eq 0){
    Write-Verbose -Message "No existing Agent ID detected, generating new GUID..."
    $guid = ([guid]::NewGuid()).Guid
}else{
    write-verbose -Message "Using existing Agent ID: $oldguid"
    $guid = $oldguid
}


Write-Verbose -Message "Defining LCM Configuration..."
Configuration LCM{
    param(
        [Parameter(Mandatory)]
        [ValidateNotNullOrEmpty()]
        [string]$Guid
    )

    LocalConfigurationManager{
        ConfigurationID = $guid
        ConfigurationModeFrequencyMins = 15
        RebootNodeIfNeeded = $False
        ConfigurationMode = "ApplyAndAutoCorrect"
        ActionAfterReboot = "ContinueConfiguration"
        RefreshMode = "Push"
        RefreshFrequencyMins = 30
        AllowModuleOverwrite = $True
        CertificateID = $Thumbprint
    }
}


write-verbose -message "Configuring LCM..."

LCM -Guid $guid -OutputPath "$($env:TEMP)\LCM"

write-verbose -message "Applying LCM configuration..."
Set-DscLocalConfigurationManager -ComputerName localhost -Path "$($env:TEMP)\LCM" | Out-Null
