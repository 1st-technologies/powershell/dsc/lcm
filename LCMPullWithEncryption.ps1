[CmdletBinding()]
param(
    [Parameter(Mandatory = $true)]
    [ValidateNotNullOrEmpty()]
    [string]$Pullserver,
    [Parameter(Mandatory = $true)]
    [ValidateNotNullOrEmpty()]
    [string]$Key,
    [Parameter(Mandatory = $true)]
    [ValidateNotNullOrEmpty()]
    [string]$Thumbprint
)

[DSCLocalConfigurationManager()]
configuration LCMPull{
    Node localhost{
        Settings{
            RefreshMode                    = 'Pull'
            RefreshFrequencyMins           = 30
            ConfigurationModeFrequencyMins = 15
            ActionAfterReboot              = "ContinueConfiguration"
            ConfigurationMode              = "ApplyAndAutoCorrect"
            RebootNodeIfNeeded             = $false
            AllowModuleOverwrite           = $true
            CertificateID                  = $Thumbprint
        }

        ConfigurationRepositoryWeb pullserver{
            ServerURL          = "$Pullserver/PSDSCPullServer.svc"
            RegistrationKey    = $Key
            ConfigurationNames = @('Demo')
        }   

        ReportServerWeb pullserver{
            ServerURL       = "Pullserver/PSDSCPullServer.svc"
            RegistrationKey = $Key
        }
    }
}

LCMPull
Set-DscLocalConfigurationManager -ComputerName localhost -Path .\LCMPull
